#!/usr/bin/env expect

# Download fabric, pre-gen spawn
set timeout 600
spawn java -jar fabric_launcher.jar
expect {
    -re {\[.+\]: Done \([0-9\.]+s\)! For help, type \"help\"} {}
    -re "Error" {
        puts "Error detected. Aborting."
        exit 1
    }
}

# Save spawn to image
send "save-all flush\r"
set timeout 30
expect {Saved the game}
send "stop\r"
