import requests
import json
from distutils.version import LooseVersion
import urllib.request


ROOT_URL = 'https://meta.fabricmc.net/v2/versions'


def download_version(version: str):
    data: json = requests.get(ROOT_URL).json()

    # Get latest stable loader
    stable_loaders = filter(lambda l: l['stable'] is True, data['loader'])
    latest_loader = max(stable_loaders, key=lambda l: LooseVersion(l['version']))
    latest_loader_version = latest_loader['version']

    # Get latest stable installer
    stable_installers = filter(lambda l: l['stable'] is True, data['installer'])
    latest_installer = max(stable_installers, key=lambda l: LooseVersion(l['version']))
    latest_installer_version = latest_installer['version']

    print(f'Found (Loader={latest_loader_version}, Installer={latest_installer_version})')

    final_url = '/'.join([ROOT_URL, 'loader', version, latest_loader_version, latest_installer_version, 'server/jar'])

    urllib.request.urlretrieve(final_url, 'fabric_launcher.jar')


if __name__ == "__main__":
    download_version('1.20.4')
