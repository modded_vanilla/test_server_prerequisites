ARG JAVA_VERSION=21

# ---------------------------------------------------------------------------- #
#              Docker image to fetch latest fabric
# ---------------------------------------------------------------------------- #
FROM eclipse-temurin:$JAVA_VERSION-jdk-alpine AS fabric_getter

# Change to root dir for easier copying
WORKDIR /

COPY get_fabric.py .env ./
RUN apk update \
    && apk add --upgrade apk-tools --no-cache py-pip \
    && apk upgrade --available --no-cache \
    && pip install --break-system-packages --no-cache-dir requests \
    && source .env \
    && python3 -c "from get_fabric import download_version; download_version('${VERS}')"


ARG JAVA_VERSION
# ---------------------------------------------------------------------------- #
#              Docker image to run automated tests in.
# ---------------------------------------------------------------------------- #
FROM eclipse-temurin:$JAVA_VERSION-jdk-alpine

# Change to test-server directory
WORKDIR /test_server

# Update system
# Fabric WEB-API: https://meta.fabricmc.net/
# Pattern: /v2/versions/loader/:game_version/:loader_version/:installer_version/server/jar
RUN apk update \
    && apk add --upgrade apk-tools --no-cache expect \
    && apk upgrade --available --no-cache

# Copy eula, server properties etc. to server folder
COPY --from=fabric_getter fabric_launcher.jar .
COPY ./requirements/ ./

# Get and install Fabric launcher, pre-generate spawn
RUN chmod -v +x ./prep_server_with_dry_run.sh\
    && ls -la . \
    && ./prep_server_with_dry_run.sh

# ---------------------------------------------------------------------------- #
#          Copy datapacks. Gitlab does this as part of the pipeline.
#            Uncomment to build the image for your own, local use.
# ---------------------------------------------------------------------------- #
#COPY shared_datapack/ world/datapacks/shared_datapack/

CMD ["java", "-Dpacktest.auto", "-Dpacktest.auto.annotations", "-Xmx2G", "-jar", "fabric_launcher.jar", "nogui"]
